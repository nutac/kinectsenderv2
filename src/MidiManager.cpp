#include "MidiManager.h"

MidiManager::MidiManager() {
}

MidiManager::~MidiManager() {
	midiOut.closePort();
}

void MidiManager::setup() {
	int port = 0;
	vector<string> portNames = midiOut.getOutPortList();

	for (size_t i = 0; i < portNames.size(); i++) {
		if (portNames[i].find("LoopBe") != string::npos) {
			port = i;
			break;
		}
	}
	midiOut.openPort(port);
}

void MidiManager::sendNoteOn(int channel, int pitch, int velocity) {
	midiOut.sendNoteOn(channel, pitch, velocity);
	//cout << "channel: " << channel << ", pitch: " << pitch << ", velocity: " << velocity << endl;
}

void MidiManager::sendNoteOff(int channel, int pitch) {
	midiOut.sendNoteOff(channel, pitch);
	//cout << "channel: " << channel << ", pitch: " << pitch << endl;

}
