#include "FluidFlow.h"

FluidFlow::FluidFlow() {
}

void FluidFlow::setup(int width, int height, string ndiSenderName) {
	//processedPixels.allocate(width, height, OF_IMAGE_GRAYSCALE);
	//processedTexture.allocate(processedPixels);

	//cout << width << ", " << height << endl;

	fbo.allocate(width, height, GL_RGBA);

	setupFluidSimulation(width, height);

	senderName = ndiSenderName;
	ndiSender.SetReadback();
	ndiSender.SetAsync();
	ndiSender.CreateSender(senderName.c_str(), width, height);

	w = width;
	h = height;

	rgbaFbo.allocate(w, h, GL_RGBA);

}

void FluidFlow::update(ofTexture& depthTexture, bool showDepth) {
	rgbaFbo.begin();
	ofClear(255, 255, 255, 255);
	depthTexture.draw(0, 0);
	rgbaFbo.end();


	float dt = 1.0 / max(ofGetFrameRate(), 1.f); // more smooth as 'real' deltaTime.

	//opticalFlow.setInput(processedTexture);
	opticalFlow.setInput(rgbaFbo.getTexture());
	opticalFlow.update();

	combinedBridgeFlow.setVelocity(opticalFlow.getVelocity());
	combinedBridgeFlow.setDensity(rgbaFbo.getTexture());
	combinedBridgeFlow.update(dt);


	fluidSimulation.addVelocity(combinedBridgeFlow.getVelocity());
	fluidSimulation.addDensity(combinedBridgeFlow.getDensity());
	fluidSimulation.update(dt);

	fbo.begin();
	ofClear(0, 0, 0, 255);
	//processedTexture.draw(0, 0);

	if (showDepth)
		rgbaFbo.draw(0, 0);


	ofEnableBlendMode(OF_BLENDMODE_ALPHA);
	//fluidSimulation.draw(0, 0, w, h);
	fluidSimulation.draw(0, 0, w, h);

	ofDisableBlendMode();

	fbo.end();
}

void FluidFlow::draw(ofRectangle targetRectangle, bool showDepth, bool showGui) {
	//fbo.begin();
	//ofClear(0, 0, 0, 255);
	////processedTexture.draw(0, 0);

	//if (showDepth)
	//	rgbaFbo.draw(0, 0);


	//ofEnableBlendMode(OF_BLENDMODE_ALPHA);
	////fluidSimulation.draw(0, 0, w, h);
	//fluidSimulation.draw(0, 0, w, h);

	//ofDisableBlendMode();

	//fbo.end();
	fbo.draw(targetRectangle);


	//ofEnableBlendMode(OF_BLENDMODE_ALPHA);
	//rgbaFbo.draw(x, 430, 512, 428);

	//fluidSimulation.draw(x-512, 430, 512, 428);
	//ofDisableBlendMode();

	if (showGui) {
		gui.draw();
	}
}

void FluidFlow::send() {
	ndiSender.SendImage(fbo);
}

void FluidFlow::setupFluidSimulation(int width, int height) {
	int w2 = width / 2;
	int h2 = height / 2;
	opticalFlow.setup(w2, h2);
	combinedBridgeFlow.setup(w2, h2, width, height);
	fluidSimulation.setup(w2, h2, width, height);

	gui.setup("settings2");
	gui.setPosition(250, 0);
	gui.add(fluidSimulation.getParameters());
	gui.add(opticalFlow.getParameters());
	gui.add(combinedBridgeFlow.getParameters());

	//if (!ofFile("settings2.xml")) { gui.saveToFile("settings2.xml"); }
	gui.loadFromFile("settings2.xml");

}
