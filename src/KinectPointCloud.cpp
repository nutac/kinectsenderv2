#include "KinectPointCloud.h"

KinectPointCloud::KinectPointCloud() {
}
void KinectPointCloud::setup(int width, int height, string ndiSenderName) {
	fbo.allocate(width, height, GL_RGBA);
	ndiSender.CreateSender(ndiSenderName.c_str(), width, height);

}

void KinectPointCloud::update(ofPixels_<unsigned short>& depthPixels, int _near, int _far, int depthMax, int step, float pointSize) {
	_pointSize = pointSize;
	bool showMesh = false;
	_showMesh = showMesh;

	int _near2 = _near * 10;
	int _far2 = _far * 10;

	int height = depthPixels.getHeight();
	int width = depthPixels.getWidth();

	pointCloud.clear();
	for (int y = 0; y < height; y += step) {
		for (int x = 0; x < width; x += step) {
			unsigned short depthValue = depthPixels[y * width + x];
			if (depthValue > _near2 && depthValue < _far2) {
				int depth = (int)ofMap(depthValue, _near2, _far2, 0, -depthMax);
				//int depth = 0;
				glm::vec3 pos(x, y, depth);

				pointCloud.addVertex(pos);
				pointCloud.addColor(ofColor(255));

			}
		}
	}

	fbo.begin();
	ofClear(0, 0, 0, 255);
	//if (_showMesh) {
	//    pointCloud.drawWireframe();
	//}
	//else {
	glPointSize(_pointSize);
	pointCloud.drawVertices();
	//}
	fbo.end();
}

void KinectPointCloud::draw(int x, int y) {
	fbo.draw(x, y);
}

void KinectPointCloud::send() {
	ndiSender.SendImage(fbo);
}
